import $ from "jquery";

$(() => {
  $("#button").click(() => {
    const component = $("#view").mcomponent({
      placeHolderId : "placeholder"
    });

    $.getJSON("http://node.skanberg.net/forecast", null, (data) => {
      component.setModel({
        forecast : data
      });
      component.render();
    });
  });
});
